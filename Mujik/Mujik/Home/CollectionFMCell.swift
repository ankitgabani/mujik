//
//  CollectionFMCell.swift
//  Mujik
//
//  Created by Gabani King on 08/04/21.
//  Copyright © 2021 Ankit Gabani. All rights reserved.
//

import UIKit

class CollectionFMCell: UICollectionViewCell {
    @IBOutlet weak var imgBg: UIImageView!
    
    @IBOutlet weak var lblName: UILabel!
    
    
}
