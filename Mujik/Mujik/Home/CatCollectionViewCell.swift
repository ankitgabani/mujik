//
//  CatCollectionViewCell.swift
//  Mujik
//
//  Created by Gabani King on 05/04/21.
//  Copyright © 2021 Ankit Gabani. All rights reserved.
//

import UIKit

class CatCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imgHome: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblDec: UILabel!
    
    
}
