//
//  HomeViewController.swift
//  Mujik
//
//  Created by Gabani King on 05/04/21.
//  Copyright © 2021 Ankit Gabani. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var collectionViewTop: UICollectionView!
    @IBOutlet weak var collectionViewFM: UICollectionView!
    @IBOutlet weak var collectionViewPlaylist: UICollectionView!
    
    @IBOutlet weak var viewFM: UIView!
    @IBOutlet weak var viewDiscovery: UIView!
    @IBOutlet weak var viewPalylist: UIView!
    @IBOutlet weak var viewSub: UIView!
    @IBOutlet weak var viewPartyroom: UIView!
    
    
    var arrTopName = ["Discover","27/7 FM","Party Room","Playlist","Subscription"]
    
    var arrImage = ["Vector-1","Group","ic_baseline-mood","Group","Group","Group"]
    var arrName = ["New Releases","Top Charts","Moods and Genres","Top Charts","Top Charts","Top Charts"]
    
    
    var selectedTopIndex = 0
    
    var flowLayoutProduct: UICollectionViewFlowLayout {
        let _flowLayout = UICollectionViewFlowLayout()
        
        _flowLayout.itemSize = CGSize(width: 140, height: 190)
        
        _flowLayout.sectionInset = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 15)
        _flowLayout.scrollDirection = UICollectionView.ScrollDirection.horizontal
        _flowLayout.minimumInteritemSpacing = 0
        _flowLayout.minimumLineSpacing = 25
        return _flowLayout
    }
    
    let sectionInsets = UIEdgeInsets(top: 10.0,
                                     left: 16.0,
                                     bottom: 12.0,
                                     right: 16.0)
    let itemsPerRow: CGFloat = 2
    
    var flowLayout: UICollectionViewFlowLayout {
        let _flowLayout = UICollectionViewFlowLayout()
        
        let paddingSpace = sectionInsets.left * (itemsPerRow + 1)
        let availableWidth = view.frame.width - paddingSpace
        let widthPerItem = availableWidth / itemsPerRow
        
        _flowLayout.itemSize = CGSize(width: widthPerItem, height: 217)
        
        _flowLayout.sectionInset = UIEdgeInsets(top: 10, left: 16, bottom: 12, right: 16)
        _flowLayout.scrollDirection = UICollectionView.ScrollDirection.vertical
        _flowLayout.minimumInteritemSpacing = 0.0
        _flowLayout.minimumLineSpacing = 16
        return _flowLayout
    }
    
    let sectionInsets1 = UIEdgeInsets(top: 10.0,
                                      left: 10,
                                      bottom: 10.0,
                                      right: 10)
    let itemsPerRow1: CGFloat = 2
    
    var flowLayout1: UICollectionViewFlowLayout {
        let _flowLayout = UICollectionViewFlowLayout()
        
        let paddingSpace = sectionInsets.left * (itemsPerRow + 1)
        let availableWidth = view.frame.width - paddingSpace
        let widthPerItem = availableWidth / itemsPerRow
        
        _flowLayout.itemSize = CGSize(width: widthPerItem, height: 250)
        
        _flowLayout.sectionInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        _flowLayout.scrollDirection = UICollectionView.ScrollDirection.vertical
        _flowLayout.minimumInteritemSpacing = 10
        _flowLayout.minimumLineSpacing = 10
        return _flowLayout
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionViewTop.delegate = self
        collectionViewTop.dataSource = self
        
        tblView.delegate = self
        tblView.dataSource = self
        
        
        collectionViewFM.delegate = self
        collectionViewFM.dataSource = self
        collectionViewFM.collectionViewLayout = flowLayout
        
        collectionViewPlaylist.delegate = self
        collectionViewPlaylist.dataSource = self
        collectionViewPlaylist.collectionViewLayout = flowLayout1
        
        
        viewFM.isHidden = true
        viewPalylist.isHidden = true
        viewDiscovery.isHidden = false
        viewSub.isHidden = true
        viewPartyroom.isHidden = true
        
        // Do any additional setup after loading the view.
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == collectionViewTop {
            return arrTopName.count
        }
        else if collectionView == collectionViewFM
        {
            return 8
        }
        else
        {
            return 8
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == collectionViewTop {
            let cell = collectionViewTop.dequeueReusableCell(withReuseIdentifier: "TopCollectionCell", for: indexPath) as! TopCollectionCell
            cell.lblNAme.text = arrTopName[indexPath.row]
            
            if selectedTopIndex == indexPath.row {
                cell.lblNAme.font = UIFont(name: "Inter-Bold", size: 24.0)
                cell.lblNAme.textColor = UIColor(red: 212/255, green: 212/255, blue: 212/255, alpha: 1)
            }
            else
            {
                cell.lblNAme.font = UIFont(name: "Inter-Regular", size: 24.0)
                cell.lblNAme.textColor = UIColor(red: 136/255, green: 136/255, blue: 136/255, alpha: 1)
            }
            
            return cell
        }
        else if collectionView == collectionViewFM
        {
            let cell = collectionViewFM.dequeueReusableCell(withReuseIdentifier: "CollectionFMCell", for: indexPath) as! CollectionFMCell
            
            return cell
        }
        else
        {
            let cell = collectionViewPlaylist.dequeueReusableCell(withReuseIdentifier: "CollectionViewPlaylistCell", for: indexPath) as! CollectionViewPlaylistCell
            
            return cell
        }
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView == collectionViewTop {
            if indexPath.row == 0
            {
                viewFM.isHidden = true
                viewPalylist.isHidden = true
                viewDiscovery.isHidden = false
                viewSub.isHidden = true
                viewPartyroom.isHidden = true
                
                
            }
            else if indexPath.row == 1
            {
                viewFM.isHidden = false
                viewPalylist.isHidden = true
                viewDiscovery.isHidden = true
                viewSub.isHidden = true
                viewPartyroom.isHidden = true
                
            }
            else if indexPath.row == 2
            {
                viewSub.isHidden = true
                viewPartyroom.isHidden = false
                viewFM.isHidden = true
                viewPalylist.isHidden = true
                viewDiscovery.isHidden = true
            }
            else if indexPath.row == 3
            {
                viewFM.isHidden = true
                viewPalylist.isHidden = false
                viewDiscovery.isHidden = true
                viewSub.isHidden = true
                viewPartyroom.isHidden = true
                
            }
            else if indexPath.row == 4
            {
                viewFM.isHidden = true
                viewPalylist.isHidden = true
                viewDiscovery.isHidden = true
                viewSub.isHidden = false
                viewPartyroom.isHidden = true
                
            }
            
        }
        
        selectedTopIndex = indexPath.row
        self.collectionViewTop.reloadData()        
    }
    
}

extension HomeViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrImage.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblView.dequeueReusableCell(withIdentifier: "tblViewCell") as! tblViewCell
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 190
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerView = Bundle.main.loadNibNamed("HomeHeaderView", owner: self, options: [:])?.first as! HomeHeaderView
        
        headerView.img.image = UIImage(named: arrImage[section])
        headerView.lblName.text = arrName[section]
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return 55
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    
}
