//
//  HomeHeaderView.swift
//  Mujik
//
//  Created by Gabani King on 06/04/21.
//  Copyright © 2021 Ankit Gabani. All rights reserved.
//

import UIKit

class HomeHeaderView: UIView {

    
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    
    @IBOutlet weak var btnViewAll: UIButton!
    
}
