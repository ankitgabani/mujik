//
//  ViewController.swift
//  Mujik
//
//  Created by Gabani King on 31/03/21.
//  Copyright © 2021 Ankit Gabani. All rights reserved.
//

import UIKit
import SVProgressHUD

class ViewController: UIViewController {
    
    //MARK:- IBOutlet
    @IBOutlet weak var imgRotect: UIImageView!
    
    //MARK:- View cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.imgRotect.rotate()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "LoginPromotVC") as! LoginPromotVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        // Do any additional setup after loading the view.
    }
    
    
}

extension UIImageView{
    func rotate() {
        let rotation : CABasicAnimation = CABasicAnimation(keyPath: "transform.rotation.z")
        rotation.toValue = NSNumber(value: Double.pi * 2)
        rotation.duration = 1
        rotation.isCumulative = true
        rotation.repeatCount = Float.greatestFiniteMagnitude
        self.layer.add(rotation, forKey: "rotationAnimation")
    }
}
